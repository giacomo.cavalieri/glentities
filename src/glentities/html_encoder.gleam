import gleam/string
import gleam/string_builder.{type StringBuilder}
import glentities/internal/string_utils

/// Encode text to be safe in the HTML body, inside element or attribute content.
///
/// `&`, `<`, `>`, `'`, and `"` are encoded.
///
/// Note! Not suitable for outputting inside `<style>`, `<script>` elements.
pub fn encode(text: String) -> String {
  text
  |> string_utils.normalise()
  |> do_encode(string_builder.new())
}

fn do_encode(text: String, acc: StringBuilder) {
  case text {
    "" -> string_builder.to_string(acc)
    "&" <> rest -> do_encode(rest, string_builder.append(acc, "&amp;"))
    "<" <> rest -> do_encode(rest, string_builder.append(acc, "&lt;"))
    ">" <> rest -> do_encode(rest, string_builder.append(acc, "&gt;"))
    "\"" <> rest -> do_encode(rest, string_builder.append(acc, "&quot;"))
    "'" <> rest -> do_encode(rest, string_builder.append(acc, "&#39;"))
    other -> {
      let maybe_grapheme = string.pop_grapheme(other)
      case maybe_grapheme {
        Ok(#(grapheme, rest)) ->
          do_encode(rest, string_builder.append(acc, grapheme))
        Error(Nil) -> string_builder.to_string(acc)
      }
    }
  }
}
