# glentities

[![Package Version](https://img.shields.io/hexpm/v/glentities)](https://hex.pm/packages/glentities)
[![Hex Docs](https://img.shields.io/badge/hex-docs-ffaff3)](https://hexdocs.pm/glentities/)

An HTML entity encoder/decoder for Gleam.

Entities can be encoded using named or hex entity references. Named entity references use the WHATWG "Named character references" list available at [https://html.spec.whatwg.org/multipage/named-characters.html#named-character-references](https://html.spec.whatwg.org/multipage/named-characters.html#named-character-references).

## Quick start

```gleam
import glentities

glentities.encode("</html>", glentities.HTMLBody) // "&lt;/html&gt;"
glentities.encode("</html>", glentities.Named)    // "&lt;&sol;html&gt;"
glentities.encode("</html>", glentities.Hex)      // "&#x3C;&#x2F;html&#x3E;"

glentities.decode("&#x3C;&#x2F;html&#x3E;")       // "</html>"
glentities.decode("&lt;&sol;html&gt;")            // "</html>"
```

### Code size

If you import `glentities`, it will pull in all the encoders and the entire named decoder. Since there are so many different entity names, this will result in a hefty JS payload. If you need to minimise the payload size, prefer importing `glentities/decoder` or one of the `glentities/*_encoder` modules directly.

## Development

```sh
gleam test  # Run the tests
```

## Installation

This package can be added to your Gleam project:

```sh
gleam add glentities
```

and its documentation can be found at <https://hexdocs.pm/glentities>.
